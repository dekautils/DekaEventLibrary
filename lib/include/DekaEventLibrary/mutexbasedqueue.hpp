#pragma once

#include "dekaeventlibrary_export.h"
#include "event.hpp"
#include "iqueue.hpp"
#include <condition_variable>
#include <mutex>
#include <queue>

namespace DekaEventLibrary {
class DEKAEVENTLIBRARY_NO_EXPORT MutexBasedQueue : public IQueue {
private:
    using Mutex = std::mutex;
    using MutexLocker = std::lock_guard<Mutex>;
    using UniqueLocker = std::unique_lock<Mutex>;
    using ConditionalVariable = std::condition_variable;

    mutable Mutex mutex;
    std::queue<Event> queue;
    ConditionalVariable popCondition;

public:
    MutexBasedQueue();

    // IQueue interface
public:
    void push(const Event& event);
    bool pop(Event& el);
    bool popWait(Event& element, std::chrono::milliseconds timeout);
    size_t size() const;
    bool empty() const;
};

}
