#pragma once

#include "dekaeventlibrary_export.h"
#include "ieventpublisher.hpp"
#include "typedefs.hpp"
#include <memory>

namespace DekaEventLibrary {

/**
 * @brief The EventPublisher class publishes messages to connected EventBus
 */
class DEKAEVENTLIBRARY_EXPORT EventPublisher : public IEventPublisher {
private:
    String topic; ///< Topic for publishing
    IEventBusPtr connectedBus; ///< Destination EventBus

public:
    EventPublisher() = default;

    void setTopic(String top) override; ///< Sets new topic
    void publish(const DataType& data) const override; ///< Publishes message to data
    void connectTo(IEventBusPtr bus) override; ///< connects publisher to EventBus
};

}
