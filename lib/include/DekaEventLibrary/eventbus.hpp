#pragma once

#include "dekaeventlibrary_export.h"
#include "ieventbus.hpp"
#include "ieventpublisher.hpp"
#include "ieventsubscriber.hpp"
#include "iqueue.hpp"
#include "typedefs.hpp"
#include <chrono>
#include <list>
#include <memory>
#include <unordered_map>

namespace DekaEventLibrary {

/**
 * @brief The EventBus class delivers events to subscribers
 */
class DEKAEVENTLIBRARY_EXPORT EventBus : public IEventBus {
private:
    using SubscriberList = std::list<IEventSubscriberPtr>;
    using SubscriberMapType = std::unordered_map<std::string, SubscriberList>;

    SubscriberMapType mapping; ///< Mapping TopicName -> Subscriber list for delivering
    IQueuePtr queue; ///< Queue for events

public:
    EventBus();

    /**
     * @brief publish publishes data in specified topic
     * @param topic topic
     * @param data data to publish
     */
    void publish(String topic, DataType data) override;

    /**
     * @brief subscribe registeres subscriber for event in specific topic
     * @param topic topic to sub
     * @param subscriber subscriber pointer. It will receive events
     */
    void subscribe(String topic, IEventSubscriberPtr subscriber) override;

    /**
     * @brief unsubscribe unregisteres subscriber for event in specific topic
     * @param topic topic to unsubscribe
     * @param subscriber subscriber pointer
     */
    void unsubscribe(String topic, IEventSubscriberPtr subscriber);
    /**
     * @brief unsubscribeAll unregisteres subscriber for all events
     * @param subscriber subscriber pointer
     */
    void unsubscribeAll(IEventSubscriberPtr subscriber);

    /**
     * @brief deliverEvents deliver events to subscribers
     * @param timelimit - max time to deliver in nanoseconds
     */
    void deliverEvents(const DurationType& timelimit = std::chrono::nanoseconds(0)) override;
};

}
