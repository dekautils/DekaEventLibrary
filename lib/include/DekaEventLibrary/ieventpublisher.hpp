#pragma once

#include "dekaeventlibrary_export.h"
#include "ieventbus.hpp"
#include "typedefs.hpp"
#include <memory>

namespace DekaEventLibrary {

class IEventPublisher;
using IEventPublisherPtr = std::shared_ptr<IEventPublisher>;

class DEKAEVENTLIBRARY_EXPORT IEventPublisher {
public:
    friend class EventBus;

    IEventPublisher() = default;
    virtual ~IEventPublisher() = default;

    virtual void setTopic(String topic) = 0;
    virtual void publish(const DataType& data) const = 0;
    virtual void connectTo(IEventBusPtr bus) = 0;
};

}
