#pragma once

#include "dekaeventlibrary_export.h"
#include <chrono>
#include <memory>

namespace DekaEventLibrary {

class IEventSubscriber;
using IEventSubscriberPtr = std::shared_ptr<IEventSubscriber>;

class Event;
class EventBus;

class DEKAEVENTLIBRARY_EXPORT IEventSubscriber {
public:
    friend class EventBus;

    IEventSubscriber() = default;
    virtual ~IEventSubscriber() = default;

    virtual bool pullEvent(Event& e) = 0;
    virtual bool pullEventWait(Event& e, std::chrono::milliseconds timeout = std::chrono::seconds(1)) = 0;

protected:
    virtual void receive(const Event& e) = 0;
};

}
