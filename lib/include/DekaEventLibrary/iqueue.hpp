#pragma once

#include "dekaeventlibrary_export.h"
#include <chrono>
#include <memory>

namespace DekaEventLibrary {

class Event;

class IQueue;
using IQueuePtr = std::shared_ptr<IQueue>;

class DEKAEVENTLIBRARY_NO_EXPORT IQueue {
public:
    IQueue() = default;
    virtual ~IQueue() = default;

    virtual void push(const Event& event) = 0;
    virtual bool pop(Event& element) = 0;
    virtual bool popWait(Event& element, std::chrono::milliseconds timeout = std::chrono::seconds(1)) = 0;

    virtual size_t size() const = 0;
    virtual bool empty() const = 0;
};

}
