#pragma once

#include "dekaeventlibrary_export.h"
#include "ieventsubscriber.hpp"
#include "typedefs.hpp"
#include <chrono>
#include <memory>

namespace DekaEventLibrary {

using DurationType = std::chrono::nanoseconds;

class IEventBus;
using IEventBusPtr = std::shared_ptr<IEventBus>;

class DEKAEVENTLIBRARY_EXPORT IEventBus {

public:
    virtual ~IEventBus() = default;

public:
    virtual void publish(String topic, DataType data) = 0;

    virtual void subscribe(String topic, IEventSubscriberPtr subscriber) = 0;
    virtual void unsubscribe(String topic, IEventSubscriberPtr subscriber) = 0;
    virtual void unsubscribeAll(IEventSubscriberPtr subscriber) = 0;
    virtual void deliverEvents(const DurationType& timelimit = std::chrono::nanoseconds(0)) = 0;
};

}
