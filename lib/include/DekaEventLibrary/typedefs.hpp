#pragma once

#include <any>
#include <string>

namespace DekaEventLibrary {

using String = std::string;
using DataType = std::any;

}
