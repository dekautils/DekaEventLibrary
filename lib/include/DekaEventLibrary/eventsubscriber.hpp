#pragma once

#include "dekaeventlibrary_export.h"
#include "ieventsubscriber.hpp"
#include "iqueue.hpp"

namespace DekaEventLibrary {

/**
 * @brief The EventSubscriber class receive event from EventBus
 */
class DEKAEVENTLIBRARY_EXPORT EventSubscriber : public IEventSubscriber {
private:
    IQueuePtr queue; ///< Event queue for processing
public:
    EventSubscriber();

    /**
     * @brief pullEvent pulls event from local queue
     * @param [out] event which filed will be filled
     * @return true if event pulled, false - otherwise
     */
    bool pullEvent(Event& event) override;

    /**
     * @brief pullEventWait pulls event from local queue if no events, waits at least timeout milleseconds
     * @param [out] e which filed will be filled
     * @param timeout max allowed time for waiting event
     * @return true if event pulled, false - otherwise
     */
    bool pullEventWait(Event& e, std::chrono::milliseconds timeout = std::chrono::seconds(1)) override;

protected:
    void receive(const Event& event) override;
};

}
