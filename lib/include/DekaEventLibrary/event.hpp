#pragma once

#include "dekaeventlibrary_export.h"
#include "typedefs.hpp"
#include <chrono>

namespace DekaEventLibrary {

using ClockType = std::chrono::high_resolution_clock;
using TimeStampType = ClockType::time_point;

/**
 * @brief The Event class represents event
 */
class DEKAEVENTLIBRARY_EXPORT Event {
private:
    String topic; ///< Topic of message
    TimeStampType timestamp; ///< Time when message happend
    DataType data; ///< Some data as string

public:
    /**
     * @brief Event creates empty event with timestamp - now
     */
    Event();
    /**
     * @brief Event creates event
     * @param t - topic
     * @param time - timestamp
     * @param d - data
     */
    Event(String t, TimeStampType time, DataType d);

    bool operator==(const Event& other) const;
    bool operator!=(const Event& other) const;

    String getTopic() const; ///< Return topic
    TimeStampType getTimestamp() const; ///< Returns TimeStamp
    DataType getData() const; ///< Return data
};

}
