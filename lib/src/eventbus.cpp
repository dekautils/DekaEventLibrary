#include "eventbus.hpp"
#include "event.hpp"
#include "eventpublisher.hpp"
#include "eventsubscriber.hpp"
#include <algorithm>

#include "mutexbasedqueue.hpp"
using QueueType = DekaEventLibrary::MutexBasedQueue;

namespace DekaEventLibrary {

EventBus::EventBus()
{
    queue = std::make_shared<QueueType>();
}

void EventBus::publish(String topic, DataType data)
{
    TimeStampType time = ClockType::now();
    Event event = Event(topic, time, data);

    queue->push(event);
}

void EventBus::subscribe(String topic, IEventSubscriberPtr subscriber)
{
    auto& list = mapping[topic];
    //Add only if not exist
    if (std::find(list.begin(), list.end(), subscriber) == list.end())
        list.push_back(subscriber);
}

void EventBus::unsubscribe(String topic, IEventSubscriberPtr subscriber)
{
    auto& list = mapping[topic];
    list.remove(subscriber);
}

void EventBus::unsubscribeAll(IEventSubscriberPtr subscriber)
{
    for (auto pair : mapping) {
        unsubscribe(pair.first, subscriber);
    }
}

void EventBus::deliverEvents(const DurationType& timelimit)
{
    bool useTimeLimit = true;
    if (timelimit == std::chrono::nanoseconds(0)) {
        useTimeLimit = false;
    }

    TimeStampType beginTime = ClockType::now();
    TimeStampType currentTime = ClockType::now();

    while (!queue->empty()) {
        Event e;
        if (!queue->pop(e)) {
            continue;
        }

        auto topic = e.getTopic();
        const auto& list = mapping[topic];

        for (const auto& subscriber : list) {
            subscriber->receive(e);
        }

        if (useTimeLimit) {
            currentTime = ClockType::now();
            DurationType dur = currentTime - beginTime;
            if (dur > timelimit)
                break;
        }
    }
}

}
