#include "event.hpp"

namespace DekaEventLibrary {

Event::Event()
    : Event("", ClockType::now(), "")
{
}

Event::Event(String t, TimeStampType time, DataType d)
{
    this->topic = std::move(t);
    this->timestamp = std::move(time);
    this->data = std::move(d);
}

bool Event::operator==(const Event& other) const
{
    return topic == other.topic && timestamp == other.timestamp && data.type() == other.data.type();
}

bool Event::operator!=(const Event& other) const
{
    return !(*this == other);
}

String Event::getTopic() const
{
    return topic;
}

TimeStampType Event::getTimestamp() const
{
    return timestamp;
}

DataType Event::getData() const
{
    return data;
}

}
