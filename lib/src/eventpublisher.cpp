#include "eventpublisher.hpp"
#include "eventbus.hpp"

namespace DekaEventLibrary {

void EventPublisher::setTopic(String top)
{
    this->topic = top;
}

void EventPublisher::publish(const DataType& data) const
{
    if (connectedBus)
        connectedBus->publish(topic, data);
}

void EventPublisher::connectTo(IEventBusPtr bus)
{
    connectedBus = bus;
}

}
