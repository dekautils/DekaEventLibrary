#include "eventsubscriber.hpp"

#include "mutexbasedqueue.hpp"
using QueueType = DekaEventLibrary::MutexBasedQueue;

namespace DekaEventLibrary {

EventSubscriber::EventSubscriber()
{
    queue = std::make_shared<QueueType>();
}

bool EventSubscriber::pullEvent(Event& event)
{
    return queue->pop(event);
}

bool EventSubscriber::pullEventWait(Event& e, std::chrono::milliseconds timeout)
{
    return queue->popWait(e, timeout);
}

void EventSubscriber::receive(const Event& event)
{
    queue->push(event);
}

}
