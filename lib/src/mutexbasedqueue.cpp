#include "mutexbasedqueue.hpp"

namespace DekaEventLibrary {

MutexBasedQueue::MutexBasedQueue()
{
}

void MutexBasedQueue::push(const Event& event)
{
    MutexLocker locker(mutex);
    queue.push(event);
    popCondition.notify_one(); ///< Allow one thread pull pushed event
}

bool MutexBasedQueue::pop(Event& el)
{
    if (empty()) {
        return false;
    }

    MutexLocker locker(mutex);
    auto out = queue.front();
    queue.pop();
    el = out;

    return true;
}

bool MutexBasedQueue::popWait(Event& element, std::chrono::milliseconds timeout)
{
    UniqueLocker locker(mutex);
    static auto waitCondition = [this]() -> bool { return !this->empty(); };

    auto cond = popCondition.wait_for(locker, timeout, waitCondition);
    if (cond) {
        auto out = queue.front();
        queue.pop();
        element = out;

        return true;
    }
    return false;
}

size_t MutexBasedQueue::size() const
{
    //MutexLocker locker(mutex);
    return queue.size();
}

bool MutexBasedQueue::empty() const
{
    //MutexLocker locker(mutex);
    return queue.empty();
}

}
