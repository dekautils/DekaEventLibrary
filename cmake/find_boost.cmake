macro(find_boost)

    find_package(Boost 1.72.0)
    if(Boost_FOUND)
        set(USE_BOOST_LOCKFREE ON CACHE INTERNAL "use boost lockfree structures for queues")
    else(Boost_FOUND)
        set(USE_BOOST_LOCKFREE OFF CACHE INTERNAL "use boost lockfree structures for queues")
    endif(Boost_FOUND)

endmacro(find_boost)
