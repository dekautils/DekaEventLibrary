#include "eventsubscriber.hpp"
#include <gtest/gtest.h>

using namespace DekaEventLibrary;

class PublicSubscriber : public EventSubscriber {

    // IEventSubscriber interface
public:
    void receive(const Event& e) override { EventSubscriber::receive(e); }
};

class TestSubscriber : public ::testing::Test {
protected:
    std::shared_ptr<PublicSubscriber> subscriber;

    // Test interface
protected:
    void SetUp() override;
    void TearDown() override;
};

void TestSubscriber::SetUp()
{
    subscriber = std::make_shared<PublicSubscriber>();
}

void TestSubscriber::TearDown()
{
    subscriber = nullptr;
}

TEST_F(TestSubscriber, PullEventsOnNewSubscriberReturnsFalse)
{
    Event e;
    ASSERT_FALSE(subscriber->pullEvent(e));
}

TEST_F(TestSubscriber, PullEventsAfterEventReceive)
{
    Event e = { "test", ClockType::now(), DataType("data") };
    subscriber->receive(e);
    Event newE;
    ASSERT_TRUE(subscriber->pullEvent(newE));

    ASSERT_EQ(newE, e);
}
