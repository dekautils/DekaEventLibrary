#include "eventpublisher.hpp"
#include "eventbus.hpp"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace DekaEventLibrary;
using namespace testing;

class MockEventBus : public EventBus {
public:
    MOCK_METHOD(void, publish, (String, DataType), (override));
};

class TestPublisher : public Test {
protected:
    std::shared_ptr<MockEventBus> bus;
    IEventPublisherPtr publisher;
    // Test interface
protected:
    void SetUp() override;
    void TearDown() override;
};

void TestPublisher::SetUp()
{
    bus = std::make_shared<MockEventBus>();
    publisher = std::make_shared<EventPublisher>();
}

void TestPublisher::TearDown()
{
    bus = nullptr;
    publisher = nullptr;
}

TEST_F(TestPublisher, NoThrowIfBusNotConnected)
{
    publisher->connectTo(nullptr);

    ASSERT_NO_THROW(publisher->publish(DataType("data")));
}

TEST_F(TestPublisher, publishEventToBus)
{
    publisher->connectTo(bus);
    publisher->setTopic("test");

    EXPECT_CALL(*bus, publish(String("test"), DataType("data")))
        .Times(Exactly(1));

    publisher->publish("data");
}
