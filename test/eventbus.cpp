#include "eventbus.hpp"
#include "event.hpp"
#include "eventpublisher.hpp"
#include "ieventsubscriber.hpp"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace DekaEventLibrary;

class TestEventBus : public ::testing::Test {
protected:
    IEventBusPtr bus;

    // Test interface
protected:
    void SetUp() override;
    void TearDown() override;
};

void TestEventBus::SetUp()
{
    bus = std::make_shared<EventBus>();
}

void TestEventBus::TearDown()
{
    bus = nullptr;
}

class MockSubscriber : public IEventSubscriber {
public:
    MOCK_METHOD(bool, pullEvent, (Event&), (override));
    MOCK_METHOD(void, receive, (const Event&), (override));
};

TEST_F(TestEventBus, deliverEventsOnce)
{
    auto subscriber = std::make_shared<MockSubscriber>();
    ON_CALL(*subscriber, receive).WillByDefault(::testing::Return());
    ON_CALL(*subscriber, pullEvent).WillByDefault(::testing::Return(true));

    bus->subscribe("test", subscriber);

    EXPECT_CALL(*subscriber, receive(::testing::_))
        .Times(::testing::Exactly(1));

    bus->publish("test", DataType("data"));

    bus->deliverEvents();
}

TEST_F(TestEventBus, deliverEventDiffTopics)
{
    auto subscriber = std::make_shared<MockSubscriber>();
    ON_CALL(*subscriber, receive).WillByDefault(::testing::Return());
    ON_CALL(*subscriber, pullEvent).WillByDefault(::testing::Return(true));

    bus->subscribe("test0", subscriber);
    bus->subscribe("test1", subscriber);
    bus->subscribe("test2", subscriber);

    EXPECT_CALL(*subscriber, receive(::testing::_))
        .Times(::testing::Exactly(3));

    bus->publish("test0", DataType("data"));
    bus->publish("test1", DataType("data"));
    bus->publish("test2", DataType("data"));

    bus->deliverEvents();
}

TEST_F(TestEventBus, subsriberReceiveOnlyForSubscribedTopic)
{
    auto subscriber1 = std::make_shared<MockSubscriber>();
    ON_CALL(*subscriber1, receive).WillByDefault(::testing::Return());
    ON_CALL(*subscriber1, pullEvent).WillByDefault(::testing::Return(true));

    auto subscriber2 = std::make_shared<MockSubscriber>();
    ON_CALL(*subscriber2, receive).WillByDefault(::testing::Return());
    ON_CALL(*subscriber2, pullEvent).WillByDefault(::testing::Return(true));

    bus->subscribe("test1", subscriber1);
    bus->subscribe("test2", subscriber2);

    EXPECT_CALL(*subscriber1, receive(::testing::_))
        .Times(::testing::Exactly(2));

    EXPECT_CALL(*subscriber2, receive(::testing::_))
        .Times(::testing::Exactly(1));

    bus->publish("test1", DataType("data"));
    bus->publish("test1", DataType("data"));
    bus->publish("test2", DataType("data"));

    bus->deliverEvents();
}
